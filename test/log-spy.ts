import {sinon} from '@loopback/testlab';
import {InMemoryLog} from './in-memory-logger';

export function createLogSpy() {
  return sinon.spy(InMemoryLog.prototype, 'add');
}

export function restoreLogSpy(spy: sinon.SinonSpy) {
  spy.restore();
}

export function createConsoleStub() {
  return sinon.stub(console, 'log');
}