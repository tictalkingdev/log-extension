import {BindingKey} from '@loopback/context';
import {TimerFn, LogFn, LogWriterFn} from './types';

/**
 * Binding keys used by this component.
 */
export namespace LOG_BINDINGS {
  export const APP_LOG_LEVEL = BindingKey.create<LOG_LEVEL>(
    'log.level',
  );
  export const TIMER = BindingKey.create<TimerFn>('log.timer');
  export const LOGGER = BindingKey.create<LogWriterFn>('log.logger');
  export const LOG_ACTION = BindingKey.create<LogFn>('log.action');
}

/**
 * The key used to store log-related via @loopback/metadata and reflection.
 */
export const LOG_METADATA_KEY = 'log.metadata';

/**
 * Enum to define the supported log levels
 */
export enum LOG_LEVEL {
  DEBUG,
  INFO,
  WARN,
  ERROR,
  OFF,
}
