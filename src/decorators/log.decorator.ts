import {LOG_LEVEL, LOG_METADATA_KEY} from '../keys';
import {
  Constructor,
  MethodDecoratorFactory,
  MetadataInspector,
  Context,
  BindingScope
} from '@loopback/context';
import {LevelMetadata} from '../types';

/**
 * Mark a controller method as requiring logging (input, output & timing)
 * if it is set at or greater than Application LogLevel.
 * LOG_LEVEL.DEBUG < LOG_LEVEL.INFO < LOG_LEVEL.WARN < LOG_LEVEL.ERROR < LOG_LEVEL.OFF
 *
 * @param level The Log Level at or above it should log
 */
export function log(level?: number) {
  if (level === undefined) level = LOG_LEVEL.WARN;
  let result = MethodDecoratorFactory.createDecorator<LevelMetadata>(
    LOG_METADATA_KEY,
    {
      level,
    },
  );
  return result;
}

/**
 * Fetch log level stored by `@log` decorator.
 *
 * @param clazz Target controller, model, repository...
 * @param methodName Target method
 */
export function getLogMetadata(
  clazz: Constructor<{}>,
  methodName: string,
): LevelMetadata {
  let result = MetadataInspector.getMethodMetadata<LevelMetadata>(
    LOG_METADATA_KEY,
    clazz.prototype,
    methodName,
  ) || {level: LOG_LEVEL.OFF};
  return result;
}