"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const keys_1 = require("./keys");
const providers_1 = require("./providers");
class LogComponent {
    constructor() {
        this.providers = {
            [keys_1.LOG_BINDINGS.TIMER.key]: providers_1.TimerProvider,
            [keys_1.LOG_BINDINGS.LOG_ACTION.key]: providers_1.LogActionProvider,
        };
    }
}
exports.LogComponent = LogComponent;
//# sourceMappingURL=component.js.map