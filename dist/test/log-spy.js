"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const testlab_1 = require("@loopback/testlab");
const in_memory_logger_1 = require("./in-memory-logger");
function createLogSpy() {
    return testlab_1.sinon.spy(in_memory_logger_1.InMemoryLog.prototype, 'add');
}
exports.createLogSpy = createLogSpy;
function restoreLogSpy(spy) {
    spy.restore();
}
exports.restoreLogSpy = restoreLogSpy;
function createConsoleStub() {
    return testlab_1.sinon.stub(console, 'log');
}
exports.createConsoleStub = createConsoleStub;
//# sourceMappingURL=log-spy.js.map